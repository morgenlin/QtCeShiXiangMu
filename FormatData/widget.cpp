﻿#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QFile>
#include <QCoreApplication>
#include <QDebug>
#include <QRegularExpression>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    setLayout(ui->verticalLayout);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_btnFile_clicked()
{
    // 获取文件路径
    QString name = QFileDialog::getOpenFileName(this);
    ui->lineEdit->setText(name);

    // 读取文件内容并显示
    ui->textEdit->clear();
    QFile file(name);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        while (!file.atEnd()) {
            QString ba = file.readLine();
            ba.replace("\n", "");
            if(!ba.isEmpty()) {
                data << ba;
                ui->textEdit->append(ba);
            }
            QCoreApplication::processEvents();
        }
    }
    else {
        qWarning("打开文件失败");
    }
}

// 去英文字母
void Widget::on_btnLetter_clicked()
{
    ui->textEdit->clear();
    for(auto i = 0; i < data.size(); i++) {
        data[i].replace(QRegularExpression("[a-z]+"), "");
        data[i].replace(QRegularExpression("[A-Z]+"), "");
        ui->textEdit->append(data[i]);
        QCoreApplication::processEvents();
    }
}

// 去符号
void Widget::on_btnSymbol_clicked()
{
    ui->textEdit->clear();
    for(auto i = 0; i < data.size(); i++) {
        data[i].replace("~", "");
        data[i].replace("||", "");
        ui->textEdit->append(data[i]);
        QCoreApplication::processEvents();
    }
}

// 去 ; 及之后应力值
void Widget::on_btnValue_clicked()
{
    on_btnSymbol_clicked();
    ui->textEdit->clear();
    for(auto i = 0; i < data.size(); i++) {
        data[i].replace(";", ",");
        data[i].replace(QRegularExpression(",[0-9]+.[0-9]+"), "");
        ui->textEdit->append(data[i]);
        QCoreApplication::processEvents();
    }
}

// 去空行
void Widget::on_btnEmptyLine_clicked()
{
}

// 去 0
void Widget::on_btnZero_clicked()
{
    ui->textEdit->clear();
    for(auto i = 0; i < data.size(); i++) {
        data[i].replace("00000", "");
        ui->textEdit->append(data[i]);
        QCoreApplication::processEvents();
    }
}
