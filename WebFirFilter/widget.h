﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWebEngineView>
#include <QtWebChannel>
#include <QJsonObject>
#include <QJsonArray>
#include "jwebchannel.h"
#include "filter.h"
#include "matrix.h"
#include "sampdataformat.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();
private slots:
    void RecvJSMeg(const QString &msg);
    void BtnReadFile_clicked();

    void BtnCalc_clicked();

    void cBoxFilterType_curIndexChanged(int index);

    void cBoxWinType_curIndexChanged(int index);

    void EditScala_editingFinished();

    void EditFreq_editingFinished();

    void EditLowerFreq_editingFinished();

    void EditUpperFreq_editingFinished();

    void BtnWebRefresh_refreshWebView();

private:
    Ui::Widget *ui;
    QWebEngineView* WebView;
    QWebChannel* WebChannel;
    JWebChannel* ChObj;
    QPushButton* BtnReadFile;
    QPushButton* BtnWebRefresh;
    QLabel* LabFilterType;
    QComboBox* cBoxFilterType;
    QHBoxLayout* hLayoutFilter;
    QLabel* LabWinType;
    QComboBox* cBoxWinType;
    QHBoxLayout* hLayoutWin;
    QLabel* LabScala;
    QLineEdit* EditScala;
    QHBoxLayout* hLayoutScala;
    QLabel* LabFreq;
    QLineEdit* EditFreq;
    QHBoxLayout* hLayoutFreq;
    QLabel* LabUpperFreq;
    QLineEdit* EditUpperFreq;
    QHBoxLayout* hLayoutUpperFreq;
    QLabel* LabLowerFreq;
    QLineEdit* EditLowerFreq;
    QHBoxLayout* hLayoutLowerFreq;
    QPushButton* BtnCalcWave;
    QVBoxLayout* vLayoutWave;
    QVBoxLayout* vLayoutSignal;
    QVBoxLayout* vLayoutOp;
    QHBoxLayout* hLayoutMain;
    SampDataFormat SampData;
    // 参数
    int FilterType;
    int WinType;
    int Scala;
    double Freq;
    double LowerFreq;
    double UpperFreq;
};

#endif // WIDGET_H
