﻿#include "langhelp.h"
#include "ui_langhelp.h"

LangHelp::LangHelp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LangHelp)
{
    ui->setupUi(this);
}

LangHelp::~LangHelp()
{
    delete ui;
}

void LangHelp::on_pushButton_clicked()
{
    accept();
}
