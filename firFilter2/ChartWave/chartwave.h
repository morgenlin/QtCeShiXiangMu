#ifndef CHARTWAVE_H
#define CHARTWAVE_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>
#include <QMouseEvent>
#include <QRubberBand>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QDropEvent>
#include "chartwave_global.h"
#include "qcustomplot.h"
#include "../Algorithm/algorithm.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class ChartWave;
}
QT_END_NAMESPACE

/**
  该库分为上下两个自定义窗口，使用 QCustomPlot 库
  上部窗口为时序波形，下部窗口为频域波形。
 */
class CHARTWAVESHARED_EXPORT ChartWave : public QWidget
{
    Q_OBJECT
public:
    enum GraphOperation {
        GO_ADD,                 // 添加曲线
        GO_DELETE,              // 删除曲线
        GO_MODIFY               // 修改信息
    };
    Q_ENUM(GraphOperation)

    /**
      定义波形曲线绑定相关信息
     */
    struct ChInfo
    {
        int     chIndex;    // 序号，自动生成
        int     chNum;      // 通道号
        QString chName;     // 通道名称
        int     freq;       // 采样频率
        QColor  color;      // 曲线颜色
        QString remark;     // 备注

        ChInfo() : chIndex(-1), chNum(-1), freq(-1), color(Qt::blue) {}
        ChInfo(int ch, const QString &name, int fs) : chIndex(-1), chNum(ch), chName(name), freq(fs), color(Qt::blue) {}
    };

    ChartWave(QWidget *parent = nullptr);
    ~ChartWave();

signals:
    void outofGraphCount();
//    void graphAction(GraphOperation type, int index, const QJsonObject &obj);
    // 时域波形轴坐标改变信号
    void timeXRangeChanged(const QCPRange &newRange);
    void timeYRangeChanged(const QCPRange &newRange);
//    void freqXRangeChanged(const QCPRange &newRange, bool fixRange);
//    void freqYRangeChanged(const QCPRange &newRange, bool fixRange);
    void freqData(int chNumber, const QVector<double> &keys, const QVector<double> &values);

public slots:
    void setFrequencyWaveVisible(bool visible);
    void setChCount(int count);
    qint8 currentChGraph() const;
    bool chNumberExist(int ch);
    void setTimeTitle(const QString &title);
    void setFreqTitle(const QString &title);
    void setTimeTitleFont(const QFont &font);
    void setFreqTitleFont(const QFont &font);
    void setTimeXRange(const QCPRange &newRange, bool fixRange = true);
    void setTimeXRange(double lower, double upper, bool fixRange = true);
    void setTimeYRange(const QCPRange &newRange, bool fixRange = true);
    void setTimeYRange(double lower, double upper, bool fixRange = true);
    void setFreqXRange(const QCPRange &newRange, bool fixRange = true);
    void setFreqXRange(double lower, double upper, bool fixRange = true);
    void setFreqYRange(const QCPRange &newRange, bool fixRange = true);
    void setFreqYRange(double lower, double upper, bool fixRange = true);
    void setTimeGraphData(int chNumber, const QVector<double> &keys, const QVector<double> &values);
    void setFreqGraphData(int chNumber, const QVector<double> &keys, const QVector<double> &values);
    // 添加一条曲线（时域和频谱曲线）
    void addGraph(const ChInfo &graphInfo);
    void deleteGraphByIndex(int index);
    void deleteGraph(int ch);

    void updateChNameByIndex(int index, const QString &name);
    void updateChName(int ch, const QString &name);

    void graphColor_triggered();
    void delGraph_triggered();
    void cleanGraph_triggered();

    // 设置滤波参数，如果不设置参数，将使用默认参数值
    void setFilterParams(FilterType type, FilterWindowType wType, int scale, qreal fUpper, qreal fLower);
    // 计算时域波形频谱
    void calcFFTWave(int ch, QVector<double> wave);

    void setEnable_Filter(bool enable);
    void setEnable_FFTCurve(bool enable);
    void setEnable_Text(bool enable);
    void setEnable_DeleteCurve(bool enable);
    void setEnable_CleanCurve(bool enable);
    void setEnable_SelectZoom(bool enable);
    void setEnable_MouseZoom(bool enable);
    void setEnable_GraphColor(bool enable);
    void setEnable_Recover(bool enable);

    void setVisible_TimeTitle(bool visible);
    void setVisible_FreqTitle(bool visible);
    void setVisible_Filter(bool visible);
    void setVisible_FFTCurve(bool visible);
    void setVisible_Text(bool visible);
    void setVisible_DeleteCurve(bool visible);
    void setVisible_CleanCurve(bool visible);
    void setVisible_SelectZoom(bool visible);
    void setVisible_MouseZoom(bool visible);
    void setVisible_GraphColor(bool visible);
    void setVisible_Recover(bool visible);

private slots:
    void action_filter_triggered();
    void action_freqWave_triggered();
    void action_flowText_triggered();
    void rightMeun_clicked(const QPoint &pos);
    void selectArea_triggered();
    // 时域波形轴坐标改变事件
    void timeWave_xRangeChanged(const QCPRange &newRange);
    void timeWave_yRangeChanged(const QCPRange &newRange);

    void actionRestore();

private:
    virtual void dragEnterEvent(QDragEnterEvent *event) override;
    virtual void dropEvent(QDropEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void mouseReleaseEvent(QMouseEvent *e);

    void showFrequencyWave(bool visible);

    void restoreTimeWave();
    void restoreFreqWave();

private:
    Ui::ChartWave   *ui;

    /**
     * index<--->ch
     * index: 曲线顺序
     * ch: 手动设置通道号
     */
    QMap<int, int>      m_chIndex;
    /**
     * ch<--->ch info
     */
    QMap<int, ChInfo>   m_chInfo;

//    Algorithm           m_alg;

    // logic
    bool m_showFreqWave;
    bool m_showFlowText;
    // 当前通道，与 graphCount 相关
    qint8           m_currentCh;
    QCPRange        m_xTimeRange;
    QCPRange        m_yTimeRange;
    QCPRange        m_xFreqRange;
    QCPRange        m_yFreqRange;
    QMap<int, qreal>m_chFreq;

    // UI
    QMenu   *m_rightClickMenu;
    QMenu   *m_menuDelGraph;
    QMenu   *m_menuFilter;
//    QAction *m_filter;
    QAction *m_freqWave;
    QAction *m_flowText;
    QAction *m_cleanGraph;
    QAction *m_selectZoom;
    QAction *m_selectDrag;
    QAction *m_restore;
    QMenu   *m_menuColor;
    QAction *m_setting;

    // 标题
    bool                    m_titleVisible1;
    bool                    m_titleVisible2;
    QCPTextElement*         m_title1;
    QCPTextElement*         m_title2;
    // 跟随线
    QList<QCPItemTracer*>   m_timeTracerList;
    QCPItemStraightLine*    m_vTimeTrackLine;
    QCPItemStraightLine*    m_hTimeTrackLine;
    QCPItemText *           m_timeText;

    QList<QCPItemTracer*>   m_freqTracerList;
    QCPItemStraightLine*    m_vFreqTrackLine;
    QCPItemStraightLine*    m_hFreqTrackLine;
    QCPItemText *           m_freqText;

    // 滤波参数
    FilterType          m_filterType;   // 滤波器类型，默认 带通
    FilterWindowType    m_winType;      // 窗口类型，默认 汉宁
    int                 m_scale;        // 阶数，默认 64
    qreal               m_freqUpper;    // 频率上限值，默认 1000
    qreal               m_freqLower;    // 频率下限值，默认 100
};

#endif // CHARTWAVE_H
