﻿#include "widget.h"
#include "ui_widget.h"

DataBaseInfo DbInfo;

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    DbInfo.Server = "127.0.0.1";
    DbInfo.User = "root";
    DbInfo.Passwd = "123456";
    DbInfo.DbName = "plat2.0alg";
    IsRun = false;
    thd1 = new Thread1;
    td1 = new QThread;
    thd1->moveToThread(td1);
    connect(td1, SIGNAL(finished()), thd1, SLOT(deleteLater()));
    connect(this, SIGNAL(StartWork()), thd1, SLOT(DoWork()));
    td1->start();
    thd2 = new Thread1;
    td2 = new QThread;
    thd2->moveToThread(td2);
    connect(td2, SIGNAL(finished()), thd2, SLOT(deleteLater()));
    connect(this, SIGNAL(StartWork()), thd2, SLOT(DoWork()));
    td2->start();
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timertimeout()));
}

Widget::~Widget()
{
    td1->quit();
    td1->wait();
    delete td1;
    td2->quit();
    td2->wait();
    delete td2;
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    if(!IsRun) {
        IsRun = true;
        ui->pushButton->setText("Stop");
        if(timer->isActive())
            timer->stop();
        timer->start(1000);
    }
    else {
        IsRun = true;
        ui->pushButton->setText("Start");
        if(timer->isActive())
            timer->stop();
    }
}

void Widget::timertimeout()
{
    emit StartWork();
}
